package com.example.valentine.chamaconnect.model;

import java.io.Serializable;

/**
 * Created by cliff on 10/01/16.
 */
public class Property implements Serializable {
    private static final long serialVersionUID = 1L;

    String location;
    String description;
    String photoId;
    String price;
    int propCode;
    int bedrooms;
    int bathrooms;
    String contact;

    public Property(){
        super();
    }

    public Property(int propCode,String location, String description, 
        int bedrooms, int bathrooms, String contact, String photoId, String price) {
        this.propCode = propCode;
        this.location = location;
        this.description = description;
        this.photoId = photoId;
        this.price = price;
        this.bedrooms = bedrooms;
        this.bathrooms = bathrooms;
        this.contact = contact;
    }

    public int getPropCode(){
        return propCode;
    }

    public void setPropCode(int propCode){
        this.propCode = propCode;
    }

    public String getPrice(){
        return price;
    }

    public void setPrice(String price){
        this.price = price;
    }


    public int getBedrooms(){
        return bedrooms;
    }

    public void setBedrooms(int bedrooms){
        this.bedrooms = bedrooms;
    }

    public int getBathrooms(){
        return bathrooms;
    }

    public void setBathrooms(int bathrooms){
        this.bathrooms = bathrooms;
    }

    public String getContact(){
        return contact;
    }

    public void setContact(String contact){
        this.contact = contact;
    }

    public String getLocation(){
        return location;
    }

    public void setLocation(String location){
        this.location = location;
    }

    public String getDescription(){
        return  description;
    }

    public void setDescription(String description){
        this.description = description;
    }

    public String getPhotoId(){
        return photoId;
    }

    public void setPhotoId(String photoId){
        this.photoId = photoId;
    }

}