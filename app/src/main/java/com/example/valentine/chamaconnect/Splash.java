package com.example.valentine.chamaconnect;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

public class Splash extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

            setContentView(R.layout.activity_splash);
            Thread splashscreen=new Thread(){
                public void run(){
                    try {
                        sleep(2000);
                    }catch (Exception e){
                        e.printStackTrace();
                    }finally {
                        startActivity(new Intent(getApplicationContext(),SignupActivity.class));
                        finish();
                    }
                }
            };

            splashscreen.start();
        }
}
