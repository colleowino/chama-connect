package com.example.valentine.chamaconnect;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.toolbox.ImageLoader;
import com.android.volley.toolbox.NetworkImageView;
import com.example.valentine.chamaconnect.helper.MySingleton;
import com.example.valentine.chamaconnect.model.Property;

import java.util.ArrayList;
import java.util.Objects;

public class ListingItem extends AppCompatActivity {
    public static final String TAG_SEL_PROPERTY = "selectedProperty";
    private Property property;

    private int propCode = 1500;

    private ImageLoader imageLoader = MySingleton.getInstance(this).getImageLoader();

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_item);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
Button loanBtn= findViewById(R.id.loan);


      loanBtn.setOnClickListener(new View.OnClickListener() {

          @Override
          public void onClick(View v) {
              Intent intent = new Intent(ListingItem.this, LoanCalcultorActivity.class);
              startActivity(intent);
          }
      });




        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        TextView location = findViewById(R.id.location);
        TextView propPrice = findViewById(R.id.propPrice);
        TextView description = findViewById(R.id.description);
        TextView bedrooms = findViewById(R.id.location_again);
        TextView bathrooms = findViewById(R.id.bathrooms);
        TextView contact = findViewById(R.id.contact);
        NetworkImageView header = findViewById(R.id.header_photo);

        Intent i = getIntent();
        property = (Property) i.getSerializableExtra(TAG_SEL_PROPERTY);

        location.setText(property.getLocation());
        description.setText(property.getDescription());
        propPrice.setText(property.getPrice());

        Log.e("DROGO", "not null " + property.getBedrooms());

        bedrooms.setText(String.valueOf(property.getBedrooms()));
        bathrooms.setText(String.valueOf(property.getBathrooms()));
        contact.setText(property.getContact());

        propCode = property.getPropCode();

        if (imageLoader == null)
            imageLoader = MySingleton.getInstance(this).getImageLoader();

        header.setImageUrl(property.getPhotoId(),imageLoader);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_cogwheel, menu);

        return true;
    }

    public void loadPhotos(){

        Log.e("DROGO", "The prop code is "+ property.getPropCode());

        ArrayList<String> images = new ArrayList<>();
        images.add("http://colleowino.github.io/hauz/img/api/props/"+propCode+"/main.jpg");
        images.add("http://colleowino.github.io/hauz/img/api/props/"+propCode+"/a.jpg");
        images.add("http://colleowino.github.io/hauz/img/api/props/"+propCode+"/b.jpg");
        images.add("http://colleowino.github.io/hauz/img/api/props/"+propCode+"/c.jpg");
        images.add("http://colleowino.github.io/hauz/img/api/props/"+propCode+"/d.jpg");
        images.add("http://colleowino.github.io/hauz/img/api/props/"+propCode+"/e.jpg");
        Intent intent = new Intent(ListingItem.this, GalleryActivity.class);
        intent.putStringArrayListExtra(GalleryActivity.EXTRA_NAME, images);
        startActivity(intent);

    }
}
